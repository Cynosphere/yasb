from core.widgets.base import BaseWidget
from core.validation.widgets.musicbee.musicbee import MUSICBEE_VALIDATION_SCHEMA
from PyQt6.QtWidgets import QLabel

from musicbeeipc.musicbeeipc import MusicBeeIPC
from musicbeeipc.enums import *

class MusicBeeWidget(BaseWidget):
    validation_schema = MUSICBEE_VALIDATION_SCHEMA

    def __init__(
            self,
            label: str,
            label_alt: str,
            update_interval: int,
            playback_icons: dict[str, str],
            callbacks: dict[str, str],
    ):
        super().__init__(update_interval, class_name="musicbee-widget")
        self._show_alt_label = False
        self._label_content = label
        self._label_alt_content = label_alt

        self._label = QLabel()
        self._label_alt = QLabel()
        self._label.setProperty("class", "label")
        self._label_alt.setProperty("class", "label alt")
        self.widget_layout.addWidget(self._label)
        self.widget_layout.addWidget(self._label_alt)
        self.register_callback("toggle_label", self._toggle_label)
        self.register_callback("update_label", self._update_label)

        self.callback_left = callbacks['on_left']
        self.callback_right = callbacks['on_right']
        self.callback_middle = callbacks['on_middle']
        self.callback_timer = "update_label"

        self._label.show()
        self._label_alt.hide()

        self._playback_icons = playback_icons

        self._ipc = MusicBeeIPC()

        self.start_timer()

    def _toggle_label(self):
        self._show_alt_label = not self._show_alt_label

        if self._show_alt_label:
            self._label.hide()
            self._label_alt.show()
        else:
            self._label.show()
            self._label_alt.hide()

        self._update_label()

    def _format_time(self, time):
        seconds = time // 1000
        hours = seconds // 3600
        seconds %= 3600
        minutes = seconds // 60
        seconds %= 60

        if seconds < 10:
            seconds = f'0{seconds}'
        if minutes < 10:
            minutes = f'0{minutes}'

        if hours != 0:
            if hours < 10:
                hours = f'0{hours}'
            return f'{hours}:{minutes}:{seconds}'
        else:
            return f'{minutes}:{seconds}'

    def _playing_state_to_name(self, state):
        if state == MBPS_Loading:
            return "loading"
        elif state == MBPS_Playing:
            return "playing"
        elif state == MBPS_Paused:
            return "paused"
        elif state == MBPS_Stopped:
            return "stopped"
        else:
            return "unknown"

    def _update_label(self):
        running = self._ipc.probe()

        play_state = self._ipc.get_play_state()
        position = self._ipc.get_position()
        duration = self._ipc.get_duration()

        artist = self._ipc.get_file_property(MBMD_Artist)
        album_artist = self._ipc.get_file_property(MBMD_AlbumArtist)
        title = self._ipc.get_file_property(MBMD_TrackTitle)
        album = self._ipc.get_file_property(MBMD_Album)

        position_formatted = self._format_time(position)
        duration_formatted = self._format_time(duration)

        # Update the active label at each timer interval
        active_label = self._label_alt if self._show_alt_label else self._label
        active_label_content = self._label_alt_content if self._show_alt_label else self._label_content
        if running:
            active_label_content = active_label_content.replace('{state}', self._playback_icons[self._playing_state_to_name(play_state)])
            active_label_content = active_label_content.replace('{artist}', artist)
            active_label_content = active_label_content.replace('{album_artist}', album_artist)
            active_label_content = active_label_content.replace('{title}', title)
            active_label_content = active_label_content.replace('{album}', album)
            active_label_content = active_label_content.replace('{position}', position_formatted)
            active_label_content = active_label_content.replace('{duration}', duration_formatted)

            active_label.setText(active_label_content)
        else:
            active_label.setText("")
