MUSICBEE_VALIDATION_SCHEMA = {
    'label': {
        'type': 'string',
        'default': 'Example Label'
    },
    'label_alt': {
        'type': 'string',
        'default': 'Example Label Alt'
    },
    'update_interval': {
        'type': 'integer',
        'default': 1000,
        'min': 0,
        'max': 60000
    },
    'playback_icons': {
        'type': 'dict',
        'schema': {
            'playing': {
                'type': 'string',
                'default': 'playing',
            },
            'paused': {
                'type': 'string',
                'default': 'paused',
            },
            'stopped': {
                'type': 'string',
                'default': 'stopped',
            },
            'loading': {
                'type': 'string',
                'default': 'loading',
            },
            'unknown': {
                'type': 'string',
                'default': 'unknown',
            }
        },
        'default': {
            'playing': 'playing',
            'paused': 'paused',
            'stopped': 'stopped',
            'loading': 'loading',
            'unknown': 'unknown',
        }
    },
    'callbacks': {
        'type': 'dict',
        'schema': {
            'on_left': {
                'type': 'string',
                'default': 'toggle_label',
            },
            'on_middle': {
                'type': 'string',
                'default': 'do_nothing',
            },
            'on_right': {
                'type': 'string',
                'default': 'toggle_label',
            }
        },
        'default': {
            'on_left': 'toggle_label',
            'on_middle': 'do_nothing',
            'on_right': 'toggle_label'
        }
    }
}
